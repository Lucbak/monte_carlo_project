import numpy as np
import matplotlib

matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from tkinter import *


class MonteCarloPI:
    def __init__(self, window):
        self.window = window
        window.geometry("500x600")
        window.title("Przybliżanie PI metodą Monte Carlo")
        window.configure(background='#3E4149')

        self.label_shots = Label(window, text="N:", background='#3E4149', foreground='white')
        self.label_shots.pack()
        self.input_shots = Entry(window, highlightbackground='#3E4149')
        self.input_shots.pack()

        self.result_text = StringVar(value="Podaj ilość prób")
        self.label_result = Label(window, textvariable=self.result_text, background='#3E4149', foreground='white')
        self.label_result.pack()

        self.btn_start = Button(window, pady=10, text="Start", width=6,
                                highlightbackground='#3E4149', command= lambda : self.plot(self.input_shots.get()))
        self.btn_start.pack()

        self.plot_object = None

    def plot(self, input_v):

        try:
            if self.plot_object:
                self.plot_object.pack_forget()

            n = int(input_v)

            x = np.random.uniform(low=-1, high=1, size=[n - 1])
            y = np.random.uniform(low=-1, high=1, size=[n - 1])
            inside_bool = x ** 2 + y ** 2 < 1
            approx_pi = 4 * np.sum(inside_bool) / n
            x_in = x[inside_bool]
            y_in = y[inside_bool]

            self.result_text.set("Uzyskane przybliżenie PI: " + str(approx_pi))

            fig = Figure(figsize=[5, 5])

            a = fig.add_subplot(111)
            a.scatter(x, y, s=1)
            a.scatter(x_in, y_in, color='r', s=1)

            canvas = FigureCanvasTkAgg(fig, master=self.window)
            self.plot_object = canvas.get_tk_widget()
            self.plot_object.pack()
            canvas.draw()


        except:
            print("Niepoprawne dane")
            return


root = Tk()
start = MonteCarloPI(root)
mainloop()
